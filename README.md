## Marvin - A ChatGPT-powered Slack bot

### Overview 
This project consists of two lambdas which talk via an SNS topic.

* https://gitlab.com/rosco.peco/chatgpt-slack-handler (this project)
* https://gitlab.com/rosco.peco/chatgpt-slack-replier

You'll need to deploy both (**important**: See below!) and set up the 
topic and the relevant IAM roles if you want to deploy this yourself.

The reason for the split is that Slack has a hard limit of three seconds
for responding, which isn't long enough for an OpenAI API request
(it often isn't even enough to spin up this lambda!).

### This Lambda

> **Note** TL;DR: Java managed Lambdas (even relatively trivial ones) are too slow for Slack;
> I was faced with two choices:
> 
> * Enable SnapStart, or
> * Switch to GraalVM native compilation.
> 
> If I were doing this again / for reals, I'd probably just use Python or something for the front-end Lambda :D
> 
> However, for the purposes of this test, I tried both, and outline some rough timing observations below.

This is the first lambda - It handles the slack slash command call.
All it really does is transform the message, push it into SNS and
`ack` back to slack.

Because of the three-second limit imposed by Slack (mentioned above), 
this lambda is **not** run in the managed Java runtime - instead, it
is compiled with GraalVM to a native (Linux) binary and run under the
`al2.provided` runtime instead. This provides a significant boost to 
cold-start speed, and execution speed in general.

The code has also been pared back and optimised for Lambda cold-start
performance (e.g. by using the `UrlConnectionHttpClient` rather than
the netty one for the Amazon SDK), however there are probably still 
gains (perhaps significant ones) to be made there.

I also found that Kotlin (unsurprisingly) increased startup time to
unacceptable levels, so this Lambda has been rewritten in Vanilla Java,
which has helped quite a bit.

> **Note** that, even with these improvements, it will still _occasionally_ take longer than three seconds, leading to a `operation_timeout` message in slack... This is however **much** less frequent than with Managed runtime.

Startup and run times for this Lambda (at cold start) are now:

```
REPORT RequestId: 588ecadd-02a2-475f-b236-1025326176d5	Duration: 2239.26 ms	Billed Duration: 2463 ms	Memory Size: 128 MB	Max Memory Used: 90 MB	
Init Duration: 222.91 ms
```

Whereas with Managed Java runtime and Kotlin in the mix, just the init was often taking more than 3 seconds:

```
REPORT RequestId: 168bce2c-e693-40dd-9241-d0d66cacf313	Duration: 9641.76 ms	Billed Duration: 9642 ms	Memory Size: 512 MB	Max Memory Used: 185 MB	
Init Duration: 3421.08 ms	
```

Compiling to native and using the managed runtime isn't _hard_ - but figuring out the
right combination of classes to put in [reflect.json](src/main/resources/reflect.json)
was a bit of a time sink, _especially_ for the Logback classes...

For comparison, enabling SnapStart on the Lambda (specifically, the Managed / Kotlin version) 
exhibited impressive init (or restore)-time improvements too: 

```
RESTORE_START Runtime Version: java:11.v18 Runtime Version ARN: arn:aws:lambda:us-east-1::runtime:9a1e27517b4463e4aed1fad35f47b787a5106d2ed775359e46836ccd433c7e45
RESTORE_REPORT Restore Duration: 313.35 ms 
```

### So I should use SnapStart?

> **TL;DR** No, just use Python (or JS or whatever) for latency-sensitive Lambda. 
> You can always do the bare-minimum in that language and farm the actual work out
> to a second Java/Kotlin/C#/whatever lambda in the background...
> 
> If you _are_ wedded to Java (et al) though, SnapStart _might_ be good enough, and is 
> significantly easier to switch on than native :D
> 
> _However_ native definitely gave the best raw results in this totally-non-scientific
> test, which could make the up-front dev cost worthwhile for your use case.

I note a few pros and cons:

* **Caveats**
  * I am on AWS free tier, I do not know if that matters for SnapStart
  * The tests below were done without any code changes to (perhaps) better suit SnapStart

* **Pros**
  * (For this particular use case) SnapStart was simply a checkbox and done
  * The improvement was gained without reverting from Kotlin to Java
  * I suspect the original Micronaut version would work with minimal changes
* **Cons**
  * SnapStart displayed very long runtimes for the first (and sometimes, second) request
    * It's worth noting this may be a code design issue rather than a SnapStart one specifically!
    * See below for example logging
  * Lambdas restored from SnapStart exhibited network instability for the first request after restoration
    * I have no idea why this is the case, but I observed it reliably (trace below)
    * Again, potentially my problem rather than SnapStart itself...

With those in mind, I decided to stick with native compilation for this Lambda, because:

* It gave better results
* I'd already done the work

#### SnapStart logs

First request after restore: 

```
REPORT RequestId: d277f1ac-8776-4287-9e9a-6ca4a7d00b71	Duration: 10155.17 ms	Billed Duration: 10374 ms	
Memory Size: 512 MB	Max Memory Used: 141 MB	

Restore Duration: 303.35 ms	Billed Restore Duration: 218 ms	
```

(Perhaps worth noting that the restore duration is billed separately)

The extended duration for this run _may_ be explained by the fact that the Lambda hit
a network timeout. This _could_ be coincidence of course, but I did observe this several
times (and never without SnapStart):

```
12:13:52.553 [main] ERROR c.s.a.b.a.SlackApiLambdaHandler - Failed to respond to a request 
(request: token=<elided....>, error: timeout)
```

### Configuration

The configuration should be set up as follows (or equivalent if using
other tooling):

![AWS Lambda Configuration Screen](images/LambdaConfig.png)

Pay particular attention to:

* the Runtime setting
* The Architecture setting - This should match the architecture of the Docker base image upon which the native binary is built (which will match your host architecture by default - so `arm64` for M1 Macs for example).

### Building

To build, you should be able to just do `./gradlew clean build`. This will 
build the application then spin up a GraalVM Docker image to build a native
binary.

It's worth noting that the GraalVM image is a bit old (`graalvm-ce:ol7-java11-22.3.1`) -
this is because the Amazon Linux 2 platform uses `glibc` 1.26, while the newer `ol8` 
GraalVM base images use 1.3x which makes the compiled binary incompatible. 

The _right_ thing to do would probably be use AL2 as the base Docker container
and install the Graal dependencies on it during the build, but I didn't go 
there (I just hacked around instead with the older base container). You _might_
choose to do it properly instead :)

Because the Docker image will (by default) match your host architecture, 
the built image will also match it (so `aarch64` A.K.A. `arm64` on M1 macs for example).
You'll need to make sure you deploy with the correct architecture on AWS.