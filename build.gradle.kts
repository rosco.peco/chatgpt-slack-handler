plugins {
    java
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("io.freefair.lombok") version "6.6.3"
}

version = "0.1"
group = "com.xdesign.rossbamford"

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.amazonaws:aws-lambda-java-events:3.11.0")
    implementation("software.amazon.awssdk:sns:2.20.18")
    implementation("software.amazon.awssdk:url-connection-client:2.20.18")

    implementation("com.formkiq:lambda-runtime-graalvm:1.1")

    implementation("org.slf4j:slf4j-simple:1.7.26")
    implementation("com.slack.api:bolt-aws-lambda:1.27.3")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.14.2")

    runtimeOnly("ch.qos.logback:logback-classic:1.4.5")

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.0")
    testImplementation("io.mockk:mockk:1.12.3")

    configurations.all {
        exclude(group = "software.amazon.awssdk", module = "apache-client")
        exclude(group = "software.amazon.awssdk", module = "netty-nio-client")
    }
}


java {
    sourceCompatibility = JavaVersion.toVersion("11")
}

tasks {
    jar {
        manifest {
            attributes["Main-Class"] = "com.formkiq.lambda.runtime.graalvm.LambdaRuntime"
        }
    }
    build {
        dependsOn()
    }
}

tasks.create("buildGraalImage") {
    inputs.files("${project.projectDir}/src/main", configurations.compileClasspath)
    outputs.upToDateWhen { file("${buildDir}/graalvm/server").exists() }
    outputs.file("${buildDir}/graalvm/server")

    doLast {
        exec {
            commandLine("bash", "-c", "./build_graalvm_native.sh")
        }
    }
}

tasks.register<Zip>("buildAwsDeployable") {
    archiveFileName.set("chatgpt-handler.zip")
    destinationDirectory.set(layout.buildDirectory)
    from(layout.projectDirectory) {
        include("bootstrap")
    }
    from(layout.buildDirectory.dir("graalvm")) {
        include("**/*")
    }
}

tasks.named("buildGraalImage") {
    dependsOn("shadowJar", "test")
}

tasks.named("buildAwsDeployable") {
    dependsOn("buildGraalImage")
}

tasks.named("build") {
    dependsOn("buildAwsDeployable")
}

