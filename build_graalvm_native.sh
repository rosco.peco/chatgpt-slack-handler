#!/bin/bash

# Using ol7-java11-22.3.1 as al2 has glibc 2.26, we need to be linked against something of that age or older
# to avoid ld errors when starting the image under al2 - This graalvm ce is based on ol7 with glibc 2.17...
docker run --rm -v "$(pwd)":/working ghcr.io/graalvm/graalvm-ce:ol7-java11-22.3.1 \
    /bin/bash -c "
                    gu install native-image; \
                    native-image --enable-url-protocols=http,https \
                      -H:ReflectionConfigurationFiles=/working/src/main/resources/reflect.json \
                      -H:ResourceConfigurationFiles=/working/src/main/resources/resources.json \
                      -H:+ReportUnsupportedElementsAtRuntime --no-server -jar \"/working/build/libs/chatgpt-handler-0.1-all.jar\" \
                    ; \
                    cp chatgpt-handler-0.1-all /working/build/graalvm/server ;\
                    cp \$JAVA_HOME/lib/security/cacerts /working/build/graalvm ;\
                    cp \$JAVA_HOME/lib/libsunec.so /working/build/graalvm"

mkdir -p build/graalvm
if [ ! -f "build/graalvm/server" ]; then
    echo "there was an error building graalvm image"
    exit 1
fi