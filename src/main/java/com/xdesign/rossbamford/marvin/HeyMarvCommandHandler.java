package com.xdesign.rossbamford.marvin;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.slack.api.app_backend.slash_commands.response.SlashCommandResponse;
import com.slack.api.bolt.context.builtin.SlashCommandContext;
import com.slack.api.bolt.handler.builtin.SlashCommandHandler;
import com.slack.api.bolt.request.builtin.SlashCommandRequest;
import com.slack.api.bolt.response.Response;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.SnsClientBuilder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HeyMarvCommandHandler implements SlashCommandHandler {
    private static final Logger LOG = LoggerFactory.getLogger(HeyMarvCommandHandler.class);
    private static final String TOPIC = System.getenv().getOrDefault("REPLIER_TOPIC_ARN", "DEFAULT_VALUE_FOR_TESTS");
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final SnsClientBuilder CLIENT_BUILDER = SnsClient.builder().httpClientBuilder(UrlConnectionHttpClient.builder());
    static final HeyMarvCommandHandler INSTANCE = new HeyMarvCommandHandler();

    @Override
    public Response apply(final SlashCommandRequest req, final SlashCommandContext ctx) {
        final var payload = req.getPayload().getText();
        LOG.debug("Incoming request from Slack: {}", payload);

        try (var client = CLIENT_BUILDER.build()) {
            client.publish(builder -> {
                    try {
                        builder.message(OBJECT_MAPPER.writeValueAsString(new SnsPrompt(payload, ctx.getResponseUrl())))
                               .topicArn(TOPIC);
                    } catch (JsonProcessingException e) {
                        LOG.error("JSON Processing exception during SNS serialization", e);
                        throw new RuntimeException(e);
                    }
                }
            );
        }

        return ctx.ack(
                SlashCommandResponse.builder()
                        .responseType("in_channel")
                        .text("Marvin will be right with you...").build()
        );
    }
}
