package com.xdesign.rossbamford.marvin;

import com.slack.api.bolt.App;
import com.slack.api.bolt.AppConfig;
import com.slack.api.bolt.aws_lambda.SlackApiLambdaHandler;
import com.slack.api.bolt.aws_lambda.request.ApiGatewayRequest;

public class SlashCommandHandler extends SlackApiLambdaHandler {

    static {
        System.setProperty("software.amazon.awssdk.http.service.impl",
                "software.amazon.awssdk.http.urlconnection.UrlConnectionSdkHttpService");
    }

    public SlashCommandHandler() {
        super(new App(new AppConfig()).command("/heymarv", HeyMarvCommandHandler.INSTANCE));
    }

    @Override
    protected boolean isWarmupRequest(ApiGatewayRequest awsReq) {
        return false;
    }
}
