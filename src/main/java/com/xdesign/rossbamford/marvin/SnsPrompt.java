package com.xdesign.rossbamford.marvin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SnsPrompt {
    @JsonProperty
    private final String prompt;

    @JsonProperty
    private final String replyUrl;
}
